import 'package:flutter/foundation.dart';

@immutable
class TextMessage {
  static const signInToSee =
      'Sign in to see photos videos and reels from your friends.';
  static const continueWithFacebook = 'Continue with Facebook';
  static const continueWithGoogle = 'Continue with Google';
  static const alreadyHaveAccount = 'Already have an account?';
  static const signIn = 'Sign in';
  static const or = 'Or';

  static const showLess = '...less';
  static const showMore = '...more';

  static const likedBy = 'Liked by';
  static const and = 'and';
  static const others = 'others';
  static const you = 'You';

  static const follow = 'Follow';
  static const unFollow = 'UnFollow';
  static const editProfile = 'Edit Profile';
  static const message = 'Message';
  static const posts = 'Posts';
  static const followers = 'Followers';
  static const following = 'Following';

  static const noPostsAvailable = "No posts";

  static const createNewPost = 'Create New Post';
  static const writeCaption = 'Write captions...';
  static const tagOthers = 'Tag others';
  static const object = 'Object';
  static const share = 'Share';

  static const loading = 'Loading';

  static const yourStory = 'Your story';

  static const comments = 'Comments';
  static const commentWithName = 'Comment with name ';
  static const noCommentAvailable = "No comments";
  static const shareWhatYouThink = "Comment here";

  static const reply = 'Reply';
  static const replyTo = 'Reply to';

  static const image = 'image';
  static const video = 'video';

  static const photoEditor = 'Photo Editor';
  static const create = 'Create';
  static const reset = 'Reset';

  static const crop = 'Crop';
  static const filters = 'Filters';
  static const adjust = 'Adjust';
  static const fit = 'Fit';
  static const tint = 'Tint';
  static const blur = 'Blur';
  static const sticker = 'Sticker';
  static const text = 'Text';
  static const draw = 'Draw';
  static const mask = 'Mask';

  static const decal = 'Decal';
  static const clamp = 'Clamp';
  static const mirror = 'Mirror';
  static const repeated = 'Repeated';

  const TextMessage._();
}
