import 'package:flutter/foundation.dart';

@immutable
class FirebaseConstants {
  static const accountExistsWithDifferentCredential =
      'account-exists-with-different-credential';

  static const emailScope = 'email';

  static const defaultName = 'No name';
  static const defaultPhoto =
      'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png';

  const FirebaseConstants._();
}
