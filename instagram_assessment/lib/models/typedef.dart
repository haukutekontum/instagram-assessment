typedef UserId = String;

typedef CommentId = String;

typedef PostId = String;

typedef ResponseId = String;

typedef IsLoading = bool;

typedef FollowId = String;