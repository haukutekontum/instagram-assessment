import 'dart:developer';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:instagram_assessment/config/core/constants/dimension.dart';
import 'package:instagram_assessment/config/core/constants/text_messages.dart';
import 'package:instagram_assessment/features/picker/controller/filter/filter_controller.dart';
import 'package:instagram_assessment/features/picker/controller/picker_controller.dart';
import 'package:instagram_assessment/features/picker/model/filter.dart';
import 'package:screenshot/screenshot.dart';

class FilterScreen extends ConsumerStatefulWidget {
  const FilterScreen({super.key});

  @override
  ConsumerState<FilterScreen> createState() => _MyWidgetState();
}

class _MyWidgetState extends ConsumerState<FilterScreen> {
  final ScreenshotController _screenshotController = ScreenshotController();
  @override
  Widget build(BuildContext context) {
    final imageProvider = ref.watch(imagePickerProvider);
    if (imageProvider == null) {
      return const Scaffold(
        body: Center(
          child:
              CircularProgressIndicator(), // Show a loading indicator while waiting for the image
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black,
        title: const Text(TextMessage.filters),
        centerTitle: true,
        leading: CloseButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        actions: [
          IconButton(
              onPressed: () async {
                Uint8List? bytes = await _screenshotController.capture();
                if (bytes != null) {
                  log('message');
                  ref.read(imagePickerProvider.notifier).updateFile(bytes);
                  if (!mounted) return;
                  Navigator.of(context).pop();
                }
              },
              icon: const Icon(Icons.done))
        ],
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Screenshot(
            controller: _screenshotController,
            child: ColorFiltered(
              colorFilter: ColorFilter.matrix(ref.watch(filterProvider).matrix),
              child: Image.memory(imageProvider),
            )),
      ),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

  Widget _buildBottomNavigationBar() {
    final filters = ref.read(filterProvider.notifier).filters();
    return Container(
      width: double.infinity,
      height: Dimension.height80,
      color: Colors.black,
      child: SafeArea(
          child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: filters.length,
        itemBuilder: (context, index) {
          final Filter filter = filters[index];
          return _bottomBarItem(
            filter: filter,
            onPress: () {
              ref.read(filterProvider.notifier).setFilter(value: filter);
            },
          );
        },
      )),
    );
  }

  Widget _bottomBarItem({
    required Filter filter,
    required VoidCallback onPress,
  }) {
    final imageProvider = ref.watch(imagePickerProvider);
    if (imageProvider == null) {
      return Container();
    }
    return InkWell(
      onTap: onPress,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5),
        child: Column(children: [
          Expanded(
            child: FittedBox(
              fit: BoxFit.cover,
              child: ColorFiltered(
                colorFilter: ColorFilter.matrix(
                  filter.matrix,
                ),
                child: Image.memory(
                  imageProvider,
                  fit: BoxFit.cover,
                  width: Dimension.width50,
                  height: Dimension.height50,
                ),
              ),
            ),
          ),
          Text(
            filter.fileName,
            style: const TextStyle(color: Colors.white),
          ),
        ]),
      ),
    );
  }
}
