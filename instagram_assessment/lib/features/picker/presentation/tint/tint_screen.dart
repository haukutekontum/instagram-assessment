import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:instagram_assessment/config/core/constants/app_colors.dart';
import 'package:instagram_assessment/config/core/constants/dimension.dart';
import 'package:instagram_assessment/config/core/constants/text_messages.dart';
import 'package:instagram_assessment/features/picker/controller/picker_controller.dart';
import 'package:instagram_assessment/features/picker/controller/tint/tint_controller.dart';
import 'package:screenshot/screenshot.dart';

class TintScreen extends ConsumerStatefulWidget {
  const TintScreen({super.key});

  @override
  ConsumerState<TintScreen> createState() => _FitScreenState();
}

class _FitScreenState extends ConsumerState<TintScreen> {
  final ScreenshotController _screenshotController = ScreenshotController();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final imageProvider = ref.watch(imagePickerProvider);
    if (imageProvider == null) {
      return const Scaffold(
        body: Center(
          child:
              CircularProgressIndicator(), // Show a loading indicator while waiting for the image
        ),
      );
    }
    final currentTint = ref.watch(tintProvider);
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black,
        title: const Text(TextMessage.tint),
        centerTitle: true,
        leading: CloseButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        actions: [
          IconButton(
            onPressed: () async {
              final bytes = await _screenshotController.capture();
              ref.read(imagePickerProvider.notifier).updateFile(bytes);
              if (mounted) {
                Navigator.of(context).pop();
              }
            },
            icon: const Icon(Icons.done),
          )
        ],
      ),
      backgroundColor: Colors.black,
      body: Stack(
        children: [
          Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: Dimension.width10),
              child: Screenshot(
                controller: _screenshotController,
                child: Image.memory(
                  ref.watch(imagePickerProvider)!,
                  fit: BoxFit.cover,
                  color: currentTint.color.withOpacity(currentTint.opacity),
                  colorBlendMode: BlendMode.color,
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Slider(
              value: currentTint.opacity,
              onChanged: (value) =>
                  ref.read(tintProvider.notifier).updatedOpacity(value: value),
              thumbColor: AppColor.facebookColor,
              activeColor: AppColor.facebookColor,
            ),
          )
        ],
      ),
      bottomNavigationBar:
          _buildBottomNavigationBar(imageProvider: imageProvider),
    );
  }

  Widget _buildBottomNavigationBar({required Uint8List imageProvider}) {
    return Container(
      width: double.infinity,
      height: Dimension.height100,
      color: Colors.black,
      child: SafeArea(
          child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: ref
              .read(colorTintListProvider)
              .map<Widget>(
                (tint) => GestureDetector(
                  onTap: () {
                    ref.read(tintProvider.notifier).updatedTint(value: tint);
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: Dimension.width7,
                        horizontal: Dimension.height10),
                    child: CircleAvatar(
                        backgroundColor:
                            ref.watch(tintProvider).color == tint.color
                                ? Colors.white
                                : Colors.transparent,
                        child: Padding(
                          padding: const EdgeInsets.all(2),
                          child: CircleAvatar(backgroundColor: tint.color),
                        )),
                  ),
                ),
              )
              .toList(),
        ),
      )),
    );
  }
}
