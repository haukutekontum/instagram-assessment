import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:instagram_assessment/config/core/constants/dimension.dart';
import 'package:instagram_assessment/config/core/constants/text_messages.dart';
import 'package:instagram_assessment/features/picker/controller/mask/mask_controller.dart';
import 'package:instagram_assessment/features/picker/controller/picker_controller.dart';
import 'package:instagram_assessment/features/picker/model/picker_type.dart';
import 'package:instagram_assessment/features/picker/presentation/mask/gesture_detector_widget.dart';
import 'package:screenshot/screenshot.dart';
import 'package:widget_mask/widget_mask.dart';

class MaskScreen extends ConsumerStatefulWidget {
  const MaskScreen({super.key});

  @override
  ConsumerState<MaskScreen> createState() => _FitScreenState();
}

class _FitScreenState extends ConsumerState<MaskScreen> {
  ScreenshotController screenshotController = ScreenshotController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final imageProvider = ref.watch(imagePickerProvider);
    if (imageProvider == null) {
      return const Scaffold(
        body: Center(
          child:
              CircularProgressIndicator(), // Show a loading indicator while waiting for the image
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black,
        title: const Text(TextMessage.mask),
        centerTitle: true,
        leading: CloseButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        actions: [
          IconButton(
            onPressed: () async {
              final bytes = await screenshotController.capture();
              ref.read(imagePickerProvider.notifier).updateFile(bytes);
              if (mounted) {
                Navigator.of(context).pop();
              }
            },
            icon: const Icon(Icons.done),
          )
        ],
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Screenshot(
                controller: screenshotController,
                child: WidgetMask(
                  blendMode: ref.watch(maskProvider),
                  mask: Stack(
                    children: [
                      Container(
                        color: Colors.black.withOpacity(0.4),
                      ),
                      GestureDetectorWidget(
                        child: Icon(
                          ref.watch(shapeMaskProvider),
                          size: 250,
                          color: Colors.white.withOpacity(1),
                        ),
                      )
                    ],
                  ),
                  child: Image.memory(
                    ref.watch(imagePickerProvider)!,
                    fit: BoxFit.cover,
                  ),
                ))),
      ),
      bottomNavigationBar:
          _buildBottomNavigationBar(imageProvider: imageProvider),
    );
  }

  Widget _buildBottomNavigationBar({required Uint8List imageProvider}) {
    return Container(
        width: double.infinity,
        height: Dimension.height120,
        color: Colors.black,
        child: SafeArea(
            child: Column(
          children: [
            Expanded(child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
              children: Mask.values.map<Widget>((mask) => TextButton(
                    onPressed: () {
                      ref.read(maskProvider.notifier).updatedBlendMode(value: getBlendMode(mask: mask));  
                    },
                    child: Text(
                      mask.name[0].toUpperCase() + mask.name.substring(1),
                      style: const TextStyle(color: Colors.white),
                    )),).toList(),
            ),
            )),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: ref
                      .read(shapeMaskProvider.notifier)
                      .shapesIcon()
                      .map((shape) => _bottomBarItem(
                            icon: shape,
                            onPress: () {
                              ref.read(shapeMaskProvider.notifier).updatedShape(value: shape);
                            },
                          ))
                      .toList(),
                ),
              ),
            ),
          ],
        )));
  }

  Widget _bottomBarItem({
    required IconData icon,
    required VoidCallback onPress,
  }) {
    return InkWell(
      onTap: onPress,
      child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: Dimension.height10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(Dimension.height8),
                child: Icon(
                  icon,
                  color: Colors.white,
                  size: Dimension.height30,
                ),
              )
            ],
          )),
    );
  }
}

BlendMode getBlendMode({required Mask mask}){
  switch (mask) {
    case Mask.dstin:
      return BlendMode.dstIn;
    case Mask.overlay:
     return BlendMode.overlay;
       case Mask.saturation:
     return BlendMode.saturation;
     case Mask.screen:
     return BlendMode.screen;
     case Mask.difference:
     return BlendMode.difference;
       case Mask.darken:
     return BlendMode.darken;
       case Mask.lighten:
     return BlendMode.lighten;
       case Mask.xor:
     return BlendMode.xor;
    default:
      return BlendMode.dstIn;
  }
}
