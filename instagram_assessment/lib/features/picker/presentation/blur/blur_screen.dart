import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:instagram_assessment/config/core/constants/app_colors.dart';
import 'package:instagram_assessment/config/core/constants/dimension.dart';
import 'package:instagram_assessment/config/core/constants/text_messages.dart';
import 'package:instagram_assessment/features/picker/controller/blur/blur_controller.dart';
import 'package:instagram_assessment/features/picker/controller/picker_controller.dart';
import 'package:instagram_assessment/features/picker/model/picker_type.dart';
import 'package:screenshot/screenshot.dart';

class BlurScreen extends ConsumerStatefulWidget {
  const BlurScreen({super.key});

  @override
  ConsumerState<BlurScreen> createState() => _FitScreenState();
}

class _FitScreenState extends ConsumerState<BlurScreen> {
  final ScreenshotController _screenshotController = ScreenshotController();

  
  TileMode tileMode = TileMode.decal;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final imageProvider = ref.watch(imagePickerProvider);
    if (imageProvider == null) {
      return const Scaffold(
        body: Center(
          child:
              CircularProgressIndicator(), // Show a loading indicator while waiting for the image
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black,
        title: const Text(TextMessage.blur),
        centerTitle: true,
        leading: CloseButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        actions: [
          IconButton(
            onPressed: () async {
              final bytes = await _screenshotController.capture();
              ref.read(imagePickerProvider.notifier).updateFile(bytes);
              if (mounted) {
                Navigator.of(context).pop();
              }
            },
            icon: const Icon(Icons.done),
          )
        ],
      ),
      backgroundColor: Colors.black,
      body: Stack(
        children: [
          Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: Dimension.height10),
              child: Screenshot(
                controller: _screenshotController,
                child: ImageFiltered(
                  imageFilter: ImageFilter.blur(
                      sigmaX: ref.watch(blurProvider).sigmaX, sigmaY: ref.watch(blurProvider).sigmaY, tileMode: tileMode),
                  child: Image.memory(
                    ref.watch(imagePickerProvider)!,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            bottom: Dimension.height40,
            left: Dimension.width0,
            right: Dimension.width0,
            child: slider(
              value: ref.watch(blurProvider).sigmaX,
              onChanged: (value) => ref.read(blurProvider.notifier).updateSigmaX(value: value),
            ),
          ),
          Positioned(
            bottom: Dimension.height80,
            left: Dimension.width0,
            right: Dimension.width0,
            child: slider(
              value: ref.watch(blurProvider).sigmaY,
              onChanged: (value) => ref.watch(blurProvider.notifier).updateSigmaY(value: value),
            ),
          )
        ],
      ),
      bottomNavigationBar:
          _buildBottomNavigationBar(imageProvider: imageProvider),
    );
  }

  Widget slider(
      {required double value, required ValueChanged<double> onChanged}) {
    return Slider(
      max: 10.0,
      min: 0.0,
      value: value,
      onChanged: onChanged,
      activeColor: AppColor.facebookColor,
      thumbColor: AppColor.facebookColor,
      label: value.toStringAsFixed(2),
    );
  }

  Widget _buildBottomNavigationBar({required Uint8List imageProvider}) {
    return Container(
        width: double.infinity,
        height: Dimension.height40,
        color: Colors.black,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: Dimension.width20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: Blurs.values.map<Widget>((blur) =>_bottomBarItem(
                color:
                    tileMode == _getTileMode(blur) ? Colors.blue : Colors.white70,
                title: blur.name,
                onPress: () {
                  setState(() {
                    tileMode = _getTileMode(blur);
                  });
                }),).toList()),
        ));
  }

   TileMode _getTileMode(Blurs blur) {
    switch (blur) {
      case Blurs.decal:
        return TileMode.decal;
      case Blurs.clamp:
        return TileMode.clamp;
      case Blurs.mirror:
        return TileMode.mirror;
      case Blurs.repeated:
        return TileMode.repeated;
      default:
        return TileMode.decal;
    }
  }

  Widget _bottomBarItem(
      {required Color color,
      required String title,
      required VoidCallback onPress}) {
    return InkWell(
      onTap: onPress,
      child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: Dimension.height10),
          child: Text(
            title,
            style: TextStyle(color: color),
          )),
    );
  }
}
