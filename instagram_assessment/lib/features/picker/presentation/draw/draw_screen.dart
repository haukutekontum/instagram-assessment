import 'dart:developer';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:instagram_assessment/config/core/constants/dimension.dart';
import 'package:instagram_assessment/config/core/constants/text_messages.dart';
import 'package:instagram_assessment/config/core/helper/pixel_helper.dart';
import 'package:instagram_assessment/features/picker/controller/draw/draw_controller.dart';
import 'package:instagram_assessment/features/picker/controller/picker_controller.dart';
import 'package:painter/painter.dart';
import 'package:screenshot/screenshot.dart';

class DrawScreen extends ConsumerWidget {
  const DrawScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ScreenshotController screenshotController = ScreenshotController();

    final imageProvider = ref.watch(imagePickerProvider);
    ref.read(drawProvider.notifier).initState();
    if (imageProvider == null) {
      return const Scaffold(
        body: Center(
          child:
              CircularProgressIndicator(), // Show a loading indicator while waiting for the image
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black,
        title: const Text(TextMessage.draw),
        centerTitle: true,
        leading: CloseButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        actions: [
          IconButton(
            onPressed: () async {
              final bytes = await screenshotController.capture();
              ref.read(imagePickerProvider.notifier).updateFile(bytes);

              Navigator.of(context).pop();
            },
            icon: const Icon(Icons.done),
          )
        ],
      ),
      backgroundColor: Colors.black,
      body: Stack(
        children: [
          Center(
            child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: Dimension.height10),
                child: Screenshot(
                    controller: screenshotController,
                    child: Stack(
                      children: [
                        Image.memory(
                          ref.watch(imagePickerProvider)!,
                          fit: BoxFit.cover,
                        ),
                        Positioned.fill(child: Painter(ref.watch(drawProvider)))
                      ],
                    ))),
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Consumer(builder: (context, ref, child) => Icon(
                        Icons.circle,
                        color: Colors.white,
                        size: ref.watch(thicknessDrawProvider) + 3,
                      ),),
                      slider(
                        value: ref.watch(thicknessDrawProvider),
                        onChanged: (value) {                  
                          ref.read(thicknessDrawProvider.notifier).setThickness(value: value);
                        },
                      )
                    ],
                  )
                ],
              )),
        ],
      ),
      bottomNavigationBar: _buildBottomNavigationBar(
          imageProvider: imageProvider, ref: ref, context: context),
    );
  }

  Widget slider(
      {required double value, required ValueChanged<double> onChanged}) {
    return Slider(
      max: 40.0,
      min: 0.5,
      value: value,
      onChanged: onChanged,
      label: value.toStringAsFixed(2),
    );
  }

  Widget _buildBottomNavigationBar(
      {required Uint8List imageProvider,
      required WidgetRef ref,
      required context}) {
    return Container(
        width: double.infinity,
        height: Dimension.height50,
        color: Colors.black,
        child: Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: _bottomBarItem(
                  icon: Icons.undo,
                  onPress: () {
                    ref.read(drawProvider).undo();
                  },
                ),
              ),
              Expanded(
                child: _bottomBarItem(
                  icon: Icons.delete,
                  onPress: () {
                    ref.read(drawProvider).clear();
                  },
                ),
              ),
              Expanded(
                child: _bottomBarItem(
                  icon: Icons.color_lens_outlined,
                  onPress: () {
                    PixelHelperImage().colorPicker(
                      context,
                      backgroundColor: ref.watch(drawProvider).drawColor,
                      onPick: (color) {
                        ref.read(drawProvider).drawColor = color;
                      },
                    );
                  },
                ),
              ),
              Expanded(
                child: _bottomBarItem(
                  icon: Icons.colorize,
                  onPress: () {
                    PixelHelperImage().show(context,
                        backgroundColor: ref.watch(drawProvider).drawColor,
                        image: imageProvider, onPick: (color) {
                      ref.read(drawProvider).drawColor = color;
                    });
                  },
                ),
              ),
              Expanded(
                  child: RotatedBox(
                quarterTurns: ref.watch(drawProvider).eraseMode ? 2 : 0,
                child: _bottomBarItem(
                  icon: Icons.create,
                  onPress: () {
                    ref.read(drawProvider).eraseMode =
                        !ref.read(drawProvider).eraseMode;
                  },
                ),
              ))
            ],
          ),
        ));
  }

  Widget _bottomBarItem(
      {required IconData icon, required VoidCallback onPress}) {
    return InkWell(
      onTap: onPress,
      child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(
              icon,
              color: Colors.white,
            ),
          )),
    );
  }
}
