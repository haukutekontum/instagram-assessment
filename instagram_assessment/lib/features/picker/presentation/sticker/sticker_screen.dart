import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:instagram_assessment/config/core/constants/dimension.dart';
import 'package:instagram_assessment/config/core/constants/text_messages.dart';
import 'package:instagram_assessment/features/picker/controller/picker_controller.dart';
import 'package:instagram_assessment/features/picker/controller/sticker/sticker_controller.dart';
import 'package:lindi_sticker_widget/lindi_controller.dart';
import 'package:lindi_sticker_widget/lindi_sticker_widget.dart';

class StickerScreen extends ConsumerStatefulWidget {
  const StickerScreen({super.key});

  @override
  ConsumerState<StickerScreen> createState() => _FitScreenState();
}

class _FitScreenState extends ConsumerState<StickerScreen> {
  late LindiController controller;

  @override
  void initState() {
    controller = LindiController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final imageProvider = ref.watch(imagePickerProvider);
    if (imageProvider == null) {
      return const Scaffold(
        body: Center(
          child:
              CircularProgressIndicator(), // Show a loading indicator while waiting for the image
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black,
        title: const Text(TextMessage.sticker),
        centerTitle: true,
        leading: CloseButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        actions: [
          IconButton(
            onPressed: () async {
              final bytes = await controller.saveAsUint8List();
              ref.read(imagePickerProvider.notifier).updateFile(bytes);
              if (mounted) {
                Navigator.of(context).pop();
              }
            },
            icon: const Icon(Icons.done),
          )
        ],
      ),
      backgroundColor: Colors.black,
      body: Stack(
        children: [
          Center(
            child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: Dimension.width10),
                child: LindiStickerWidget(
                  controller: controller,
                  child: SizedBox(
                    width: double.infinity,
                    height: double.infinity,
                    child: Image.memory(
                      ref.watch(imagePickerProvider)!,
                      fit: BoxFit.cover,
                    ),
                  ),
                )),
          ),
        ],
      ),
      bottomNavigationBar:
          _buildBottomNavigationBar(imageProvider: imageProvider),
    );
  }

  Widget _buildBottomNavigationBar({required Uint8List imageProvider}) {
    final int category = ref.watch(stickerProvider);
    final List<List<String>> stickers = ref.watch(stickersListProvider);
    return Container(
        width: double.infinity,
        height: Dimension.height150,
        color: Colors.black,
        child: SafeArea(
            child: Column(
          children: [
            Expanded(
                child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: stickers[category].length,
              itemBuilder: (context, idx) {
                final String sticker = stickers[category][idx];
                return _iconItem(
                    sticker: sticker,
                    onPress: () {
                      setState(() {
                        controller.addWidget(Image.asset(sticker));
                      });
                    });
              },
            )),
            Row(
              children: stickers
                  .map((sticker) => _bottomBarItem(
                        icon: sticker[0],
                        onPress: () {
                          ref
                              .read(stickerProvider.notifier)
                              .getCategory(value: stickers.indexOf(sticker));
                        },
                        idx: stickers.indexOf(sticker),
                      ))
                  .toList(),
            )
          ],
        )));
  }

  Widget _bottomBarItem(
      {required String icon, required VoidCallback onPress, required int idx}) {
    return InkWell(
      onTap: onPress,
      child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: Dimension.width10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                color: idx == ref.watch(stickerProvider)
                    ? Colors.blue
                    : Colors.transparent,
                height: Dimension.height2,
                width: Dimension.width30,
                padding:
                    const EdgeInsets.symmetric(horizontal: Dimension.width7),
              ),
              Padding(
                padding: const EdgeInsets.all(Dimension.height8),
                child: Image.asset(
                  icon,
                  width: Dimension.width40,
                ),
              )
            ],
          )),
    );
  }

  Widget _iconItem({required String sticker, required onPress}) {
    return InkWell(
      onTap: onPress,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: Dimension.width5),
        child: FittedBox(
          fit: BoxFit.cover,
          child: Image.asset(
            sticker,
            fit: BoxFit.cover,
            width: Dimension.width50,
            height: Dimension.height50,
          ),
        ),
      ),
    );
  }
}
