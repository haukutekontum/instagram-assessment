import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:painter/painter.dart';

final drawProvider =
    StateNotifierProvider<DrawImageController, PainterController>(
  (ref) => DrawImageController(),
);

class DrawImageController extends StateNotifier<PainterController> {
  DrawImageController() : super(PainterController());

  void initState() {
    state.backgroundColor = Colors.transparent;
  }
}

final thicknessDrawProvider =
    StateNotifierProvider<ThicknessDrawController, double>(
  (ref) => ThicknessDrawController(ref: ref),
);

class ThicknessDrawController extends StateNotifier<double> {
  final Ref _ref;
  ThicknessDrawController({required Ref ref})
      : _ref = ref,
        super(ref.watch(drawProvider).thickness);

  void setThickness({required value}) {
    state = value;
    _ref.read(drawProvider).thickness = value;
  }
}
