class Sigma {
  double sigmaX;
  double sigmaY;

  Sigma({required this.sigmaX, required this.sigmaY});

  Sigma.unFollow():sigmaX=0.0, sigmaY=0.0;

  Sigma setSigmaX(double value) {
    return Sigma(sigmaX: value, sigmaY: sigmaY);
  }

  Sigma setSigmaY(double value) {
    return Sigma(sigmaX: sigmaX, sigmaY: value);
  }
}
