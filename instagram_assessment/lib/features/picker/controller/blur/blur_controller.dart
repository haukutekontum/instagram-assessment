import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:instagram_assessment/features/picker/controller/blur/sigma.dart';

final blurProvider = StateNotifierProvider<BlurImageController, Sigma>(
  (ref) => BlurImageController(),
);

class BlurImageController extends StateNotifier<Sigma> {
  BlurImageController() : super(Sigma.unFollow());

  void updateSigmaX({required double value}) => state = state.setSigmaX(value);
  void updateSigmaY({required double value}) => state = state.setSigmaY(value);
}
