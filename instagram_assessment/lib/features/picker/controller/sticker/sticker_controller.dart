import 'package:flutter_riverpod/flutter_riverpod.dart';

final stickerProvider = StateNotifierProvider<StickerImageController, int>(
  (ref) => StickerImageController(),
);

final stickersListProvider = Provider<List<List<String>>>(
  (ref) => StickerImageController().listStickers(),
);


class StickerImageController extends StateNotifier<int> {
  StickerImageController() : super(0);

  void getCategory({required int value}) => state = value;

  List<List<String>> listStickers() => [
        _generateStickerList(category: 'dog', count: 5),
        _generateStickerList(category: 'nature', count: 5),
        _generateStickerList(category: 'girl', count: 5),
        _generateStickerList(category: 'food', count: 5),
      ];

  List<String> _generateStickerList(
      {required String category, required int count}) {
    List<String> list = [];
    for (int i = 1; i <= count; i++) {
      list.add('assets/stickers/${category}_$i.png');
    }
    return list;
  }
}
