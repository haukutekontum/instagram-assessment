import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final maskProvider = StateNotifierProvider<MaskImageController, BlendMode>(
  (ref) => MaskImageController(),
);

final shapeMaskProvider = StateNotifierProvider<ShapeMaskImageController, IconData>(
  (ref) => ShapeMaskImageController(),
);

class MaskImageController extends StateNotifier<BlendMode> {
  MaskImageController() : super(BlendMode.dstIn);  

   void updatedBlendMode({required BlendMode value}) => state = value;
}

class ShapeMaskImageController extends StateNotifier<IconData> {
  ShapeMaskImageController() : super(Icons.square,);  

   List<IconData> shapesIcon(){
    return[
      Icons.favorite,
      Icons.star,
      Icons.circle,
      Icons.square,
      Icons.square_outlined,
      Icons.sunny,
      Icons.ac_unit,
    ];
  }

  void updatedShape({required IconData value}) => state = value;
}
