import 'package:flutter_riverpod/flutter_riverpod.dart';

final showEditorProvider = StateNotifierProvider<FontImageController, bool>(
  (ref) => FontImageController(),
);

final fontListProvider = Provider<List<String>>(
  (ref) => FontImageController().fontsList(),
);

class FontImageController extends StateNotifier<bool> {
  FontImageController() : super(false); 

  void updatedShowEditor({required bool value}) => state = value;

  List<String> fontsList() {
    return [
      'Aloevera',
      'CallOfOpsDutyIi',
      'Cookiemonster',
      'MajorLeagueDuty',
      'ModernWarfare',
      'MoonkidsPersonalUseExtbd',
      'MouldyCheeseRegular',
      'NatureBeautyPersonalUse',
    ];
  }
}
