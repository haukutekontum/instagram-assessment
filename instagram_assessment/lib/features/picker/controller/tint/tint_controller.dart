import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:instagram_assessment/features/picker/model/tint.dart';

final tintProvider = StateNotifierProvider<TintImageController, Tint>(
  (ref) => TintImageController(),
);

final colorTintListProvider = Provider<List<Tint>>(
  (ref) => TintImageController().colorList(),
);

class TintImageController extends StateNotifier<Tint> {
  TintImageController() : super(Tint(color: Colors.orange),);

  void updatedTint({required Tint value}) => state = value;

  void updatedOpacity({required double value}) =>
    state = Tint(color: state.color, opacity: value);

  List<Tint> colorList() {
    return [
      Tint(color: Colors.orange),
      Tint(color: Colors.green),
      Tint(color: Colors.yellow),
      Tint(color: Colors.blue),
      Tint(color: Colors.pink),
      Tint(color: Colors.red),
      Tint(color: Colors.purple),
    ];
  }
}
