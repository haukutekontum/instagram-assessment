class Filter {
  final String fileName;
  final List<double> matrix;

  Filter.unknown()
      : fileName = 'No Filter',
        matrix = [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0];

  Filter({required this.fileName, required this.matrix});
}
