import 'package:flutter/material.dart';

class Tint {
  final Color color;
  double opacity;

  Tint({required this.color, this.opacity = 0});
}
