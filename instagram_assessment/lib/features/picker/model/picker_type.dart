enum Picker {
  crop,
  filter,
  adjust,
  fit,
  tint,
  blur,
  sticker,
  text,
  draw,
  mask,
}

enum Fit {
  ratio,
  blur,
  color,
}

enum Adjust {
  brightness,
  contrast,
  saturation,
  hue,
  sepia,
}

enum Blurs {
  decal,
  clamp,
  mirror,
  repeated,
}

enum Mask {
  dstin,
  overlay,
  screen,
  saturation,
  difference,
  darken,
  lighten,
  xor,
}
