import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:instagram_assessment/features/post/presentation/view/detail/elements/tiles/elements/user_image_view.dart';
import 'package:instagram_assessment/features/user/controller/user_controller.dart';
import 'package:instagram_assessment/models/typedef.dart';
import 'package:routemaster/routemaster.dart';

class UserProfileTile extends ConsumerWidget {
  final UserId userId;

  const UserProfileTile({required this.userId, super.key});

  void _navigateToUserDetail(
          {required BuildContext context, required String uid}) =>
      Routemaster.of(context).push('/u/$uid');

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final userInfo = ref.watch(userByIdProvider(userId));

    return userInfo.when(
      data: (user) {
        return ListTile(
          leading: GestureDetector(
            onTap: () => _navigateToUserDetail(context: context, uid: userId),
            child: UserImageView(
              imageUrl: user.image,
            ),
          ),
          title: Text(
            user.name,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          trailing: IconButton(
            onPressed: () {},
            icon: const Icon(Icons.more_vert),
          ),
        );
      },
      error: (error, stackTrace) => const Text('Error'),
      loading: () => const Text('Loading'),
    );
  }
}
