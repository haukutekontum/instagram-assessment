import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:instagram_assessment/config/core/constants/assets_path.dart';
import 'package:instagram_assessment/config/core/constants/dimension.dart';
import 'package:instagram_assessment/features/user/controller/user_controller.dart';
import 'package:instagram_assessment/features/user/presentation/view/detail/appbar/user_detail_appbar.dart';
import 'package:instagram_assessment/features/user/presentation/view/detail/component/button/toggle_follow_button.dart';
import 'package:instagram_assessment/features/user/presentation/view/detail/component/show_all_posts_grid_view.dart';
import 'package:instagram_assessment/features/user/presentation/view/detail/component/header/user_info_header.dart';
import 'package:instagram_assessment/features/user/presentation/view/detail/component/show_all_followings.dart';
import 'package:instagram_assessment/features/user/presentation/view/detail/component/style/user_detail_style.dart';
import 'package:instagram_assessment/models/typedef.dart';

class UserDetailsView extends ConsumerStatefulWidget {
  final UserId userId;
  const UserDetailsView({super.key, required this.userId});

  @override
  ConsumerState<UserDetailsView> createState() => _UserMainViewState();
}

class _UserMainViewState extends ConsumerState<UserDetailsView> {
  @override
  Widget build(BuildContext context) {
    final userInfo = ref.watch(userByIdProvider(widget.userId));

    return userInfo.when(
      data: (uInfo) => Scaffold(
        body: DefaultTabController(
          length: Dimension.tabLengthDefault,
          child: NestedScrollView(
              headerSliverBuilder: (context, innerBoxIsScrolled) {
                return [
                  UserDetailAppbar(name: uInfo.name),
                  SliverList(
                    delegate: SliverChildListDelegate(
                      [
                        UserInfoHeader(uInfo: uInfo),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            //FollowButton
                            uInfo.uid == ref.watch(userProvider)
                                ? TextButton(
                                    onPressed: () async {},
                                    child: UserDetailStyle.editButton)
                                : ToggleFollowButton(uInfo: uInfo),
                            TextButton(
                                onPressed: () {},
                                child: UserDetailStyle.messageButton),
                            uInfo.uid == ref.watch(userProvider)
                                ? const SizedBox()
                                : TextButton(
                                    onPressed: () {},
                                    child: UserDetailStyle.addFollowButton,
                                  ),
                          ],
                        ),
                         ShowAllFollowings(uDocumentId: uInfo.documentId),
                      ],
                    ),
                  ),
                  SliverPersistentHeader(
                    delegate: _SliverAppBarDelegate(
                      TabBar(
                        tabs: [
                          Tab(child: Image.asset(AssetsPath.exportMenuButton)),
                          Tab(child: Image.asset(AssetsPath.reelVideoButton)),
                          Tab(child: Image.asset(AssetsPath.personalButton)),
                        ],
                      ),
                    ),
                    pinned: true,
                  )
                ];
              },
              body: Container(
                color: Colors.white,
                child: TabBarView(
                  children: [
                    ShowAllPostGridView(uid: uInfo.uid),
                    ShowAllPostGridView(uid: uInfo.uid),
                    ShowAllPostGridView(uid: uInfo.uid),
                  ],
                ),
              )),
        ),
      ),
      error: (error, stackTrace) => const Text('Error'),
      loading: () => const Text('Loading'),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final TabBar _tabBar;

  _SliverAppBarDelegate(this._tabBar);

  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  double get minExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      color: const Color.fromRGBO(255, 255, 255, 1),
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}
